pmVIDEO@HOME
=============
> pmVIDEO@HOME is a project consisting of a video-sharing site builder and a distributed video encoding application.
The pmVIDEO@HOME video-sharing site builder will automatically create a web site for people to watch and share videos.
Users will be able to upload videos freely, regardless of video type or captions.
pmVIDEO@HOME’s distributed video encoding application operates on each client's system, downloading randomly selected videos from the server and then automatically encoding them in designated formats.
The encoded videos will then be uploaded back to the server, making it available for others to watch.