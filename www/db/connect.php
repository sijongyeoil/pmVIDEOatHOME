<?php
    $db = json_decode(file_get_contents($root_path . '/db/config.json'), true);
	$db['table_prefix'] = 'pm_';
	try {
		$mysqli = @mysqli_connect($db['hostname'], $db['user'], $db['password']);
		if(!$mysqli) {
			throw new Exception('ERR:DBCON');
		} else {
			mysqli_query($mysqli, 'set names utf8');
			mysqli_select_db($mysqli, $db['database']);
		}
	}
	catch(Exception $e) {
		unlink($root_path . '/db/config.json');
		header('Refresh:0');
	}