<?php
	$db_part_path = str_replace("\\" . basename(__FILE__), "", realpath(__FILE__));

    if((!isset($_POST['hostname']))
    || (!isset($_POST['user']))
    || (!isset($_POST['password']))
    || (!isset($_POST['database']))) {
        http_response_code(400);
		echo "Missing Required Parameter";
		print_r($_POST);
		die();
    }

    $f_config = fopen($db_part_path . '/config.json', 'w');
    $m_config = json_encode($_POST);
    fwrite($f_config, $m_config);
    fclose($f_config);

	header('Refresh:0; url=../');
    exit();