<?php
	$root_path = $_SERVER['DOCUMENT_ROOT'];

    try {
        if (!file_exists('./db/config.json')) {
            require('./db/setting.html');
            exit();
        } else {
            require('./db/connect.php');
        }
    } catch (Exception $e) {
        echo $e;
    }